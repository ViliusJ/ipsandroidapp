package lt.kvk.i12.jankauskas.v.ips_application.models;

import java.util.Date;

public class Violation {

    private long id;

    private String objectCode;

    private String objectType;

    private String objectName;

    private String objectAccessLevel;

    private Date violationDateTime;

    private String restrictedArea;

    private String planName;

    public Violation() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectAccessLevel() {
        return objectAccessLevel;
    }

    public void setObjectAccessLevel(String objectAccessLevel) {
        this.objectAccessLevel = objectAccessLevel;
    }

    public Date getViolationDateTime() {
        return violationDateTime;
    }

    public void setViolationDateTime(Date violationDateTime) {
        this.violationDateTime = violationDateTime;
    }

    public String getRestrictedArea() {
        return restrictedArea;
    }

    public void setRestrictedArea(String restrictedArea) {
        this.restrictedArea = restrictedArea;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }
}
