package lt.kvk.i12.jankauskas.v.ips_application.network;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class GetRequest extends AsyncTask<String, Integer, String> {

    protected String doInBackground(String... urls) {
        HttpURLConnection connection = null;
        try {
            StringBuilder sb = new StringBuilder();
            URL url = new URL(urls[0]);
            connection = (HttpURLConnection) url.openConnection();
//            connection.setRequestMethod("GET");
//            connection.setDoOutput(true);
//            connection.setConnectTimeout(50000);
//            connection.setReadTimeout(50000);
//            connection.connect();
            InputStream in = new BufferedInputStream(connection.getInputStream());
            BufferedReader bin = new BufferedReader(new InputStreamReader(in));
            String inputLine;
            while ((inputLine = bin.readLine()) != null) {
                sb.append(inputLine);
            }
            connection.disconnect();
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("PrintOut" , "request Failed"+ e);
        }
         finally {
            // regardless of success or failure, we will disconnect from the URLConnection.
            connection.disconnect();
        }

        return null;
    }

    protected void onProgressUpdate(Integer... progress) {
    }

//    @RequiresApi(api = Build.VERSION_CODES.N)
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }


}



