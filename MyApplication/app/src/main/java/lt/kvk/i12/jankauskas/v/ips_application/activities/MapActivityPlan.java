package lt.kvk.i12.jankauskas.v.ips_application.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.Toast;

import com.davemorrissey.labs.subscaleview.ImageSource;

import java.util.ArrayList;
import java.util.List;

import lt.kvk.i12.jankauskas.v.ips_application.R;
import lt.kvk.i12.jankauskas.v.ips_application.api.LogAPI;
import lt.kvk.i12.jankauskas.v.ips_application.api.PlanAPI;
import lt.kvk.i12.jankauskas.v.ips_application.models.Log;
import lt.kvk.i12.jankauskas.v.ips_application.models.Plan;
import lt.kvk.i12.jankauskas.v.ips_application.models.TracableObject;
import lt.kvk.i12.jankauskas.v.ips_application.views.PinView;

public class MapActivityPlan extends AppCompatActivity {

    public volatile boolean mapIsReady = false;
    public PinView imageView;
    float x = 0;
    float y =0;
    public LogAPI logAPI;
    public PlanAPI planAPI;

    public Thread threadLog;
    public Thread threadPlan;

    public int planWidth =1;
    public int planHeight=1;
    public int screenWidth =1;
    public int screenheight =1;
    public volatile boolean stopThread = false;
    public Plan plan;
    public TracableObject user;
    public volatile Bitmap imageString;
    public List<String> pinNames = new ArrayList<>();
    public String pinNameUser ="";
    public long planId;



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        long defLong = 3;
        planId = intent.getLongExtra( "Plan Id",3);

        setupToolbar();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);


        imageView = findViewById(R.id.imageView);

        logAPI = new LogAPI();
        planAPI = new PlanAPI();
        user = getCurrentUser();


        threadPlan = new Thread()
        {
            public void run() {
                startGetPlan(planId);
            }
        };
        threadPlan.start();

        threadLog = new Thread()
        {
            public void run() {
                startLogSearch();
            }
        };
        threadLog.start();
    }

    public TracableObject getCurrentUser() {
        TracableObject tempUser = new TracableObject();
        long tempID = 0;
        String tempNAME ="";
        String tempCODE ="";
        String tempACCESS ="";
        String tempTYPE ="";
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        tempUser.setId(sharedPref.getLong("ID",tempID));
        tempUser.setObjName(sharedPref.getString("NAME",tempNAME));
        tempUser.setObjectCode(sharedPref.getString("CODE",tempCODE));
        tempUser.setAccessLevel(sharedPref.getString("ACCESS",tempACCESS));
        tempUser.setObjType(sharedPref.getString("TYPE",tempTYPE));

//        Toast.makeText(MapActivityPlan.this, "User from memory : "+ tempUser.getObjectCode(),Toast.LENGTH_SHORT).show();

        return tempUser;
    }

    public void startGetPlan(long planId){


            Plan plan = planAPI.getPlanById(planId);

            Intent intent = new Intent("lt.kvk.i12.jankauskas.v.PLAN_SERVICE2");
            intent.putExtra("Plan", plan);
            String image = konvertString(plan.getPlanImage());
            byte[] imageByteArray = Base64.decode(image, Base64.DEFAULT);
            Bitmap decoded = BitmapFactory.decodeByteArray(imageByteArray, 0, imageByteArray.length);
            imageString = decoded;
            sendBroadcast(intent);
    }





    public String konvertString(String imageDataString){
        String image = imageDataString;
        if(image.contains("data:image")){

            image= image.substring(22,image.length());
        }
        return image;





    }







    public void startLogSearch (){
        while (true) {
            if (stopThread) {
                return;
            }
            if (mapIsReady) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // Process exception
                }
                Intent intent = new Intent("lt.kvk.i12.jankauskas.v.ALL_LOGS_SERVICE2");
                ArrayList<Log> allLogs = new ArrayList<Log>(logAPI.getLiveLogListByPlanName(plan.getPlanName()));
                if (allLogs.size() != 0) {
                    intent.putParcelableArrayListExtra("Logs" ,allLogs);
                    sendBroadcast(intent);
                } else {

                }
            }
        }

    }

    private BroadcastReceiver planReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            plan =intent.getParcelableExtra("Plan");
            planHeight = plan.getPlanHeight();
            planWidth = plan.getPlanWidth();
//            imageView.setImage(ImageSource.bitmap(decoded));
//            imageView.setPinUser(new PointF(x, y), "pin1");
            imageView.setImage(ImageSource.bitmap(imageString));
            mapIsReady = true;
            Toast.makeText(MapActivityPlan.this, "Plan receiver: ", Toast.LENGTH_SHORT).show();


        }
    };

    private BroadcastReceiver logReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            Toast.makeText(MapActivityUser.this, "Broadcast received", Toast.LENGTH_SHORT).show();
           ArrayList<Log> logs = intent.getParcelableArrayListExtra("Logs");
           // remove all the pins
           for (int j =0;pinNames.size()>j;j++){
               imageView.removeOPin(pinNames.get(j));
           }
           if (!pinNameUser.equals("")){
               imageView.removeUPin(pinNameUser);
           }
           pinNameUser = "";
           pinNames = new ArrayList<>();
           for(int i =0; logs.size()>i;i++){
               if(logs.get(i).getObjectId().equals(user.getObjectCode())){
                   pinNameUser = user.getObjectCode();
                   imageView.setPinUser(new PointF(logs.get(i).getCoordinateX(), logs.get(i).getCoordinateY()), pinNameUser);
               }else {
                   String pinName = "pin" + i;
                   pinNames.add(pinName);
                   imageView.setPinObject(new PointF(logs.get(i).getCoordinateX(), logs.get(i).getCoordinateY()), pinName);
               }
           }
//            Toast.makeText(MapActivityUser.this, "Broadcast received :" +xCoord+" "+yCoord, Toast.LENGTH_SHORT).show();


        }
    };

    @Override
    protected void onResume()
    {
        super.onResume();
        stopThread = false;
        mapIsReady = false;

        IntentFilter planFilter = new IntentFilter("lt.kvk.i12.jankauskas.v.PLAN_SERVICE2");
        registerReceiver(planReceiver,planFilter);


        IntentFilter logFilter = new IntentFilter("lt.kvk.i12.jankauskas.v.ALL_LOGS_SERVICE2");
        registerReceiver(logReceiver,logFilter);

    }
    //
    @Override
    protected void onPause() {
        stopThread = true;

        unregisterReceiver(planReceiver);
        unregisterReceiver(logReceiver);

        super.onPause();
    }
}
