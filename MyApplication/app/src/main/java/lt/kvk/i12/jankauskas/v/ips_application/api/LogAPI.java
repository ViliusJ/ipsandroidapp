package lt.kvk.i12.jankauskas.v.ips_application.api;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import lt.kvk.i12.jankauskas.v.ips_application.Environment;
import lt.kvk.i12.jankauskas.v.ips_application.models.Log;
import lt.kvk.i12.jankauskas.v.ips_application.network.GetRequest;

public class LogAPI {


    public List<Log> getLogListByObjectId( long objectId){
        List<Log> allLogs = new ArrayList<Log>();
        String result = null;
        try {
            GetRequest getRequest = new GetRequest();
            result = getRequest.execute(Environment.URL.getUrl() +"/log/object/"+objectId).get();

            JSONArray resultArray = new JSONArray(result);
            for (int i = 0; i < resultArray.length(); i++) {
                Log tempLog = new Log();
                JSONObject log = resultArray.getJSONObject(i);
                tempLog.setId(log.getLong("id"));
                tempLog.setCoordinateX(BigDecimal.valueOf(log.getDouble("coordinateX")).floatValue());
                tempLog.setCoordinateY(BigDecimal.valueOf(log.getDouble("coordinateY")).floatValue());

//                // Getting dateTime from JSONObject
//                String dateStr = log.getString("regDateTime");
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                Date regDateTime = sdf.parse(dateStr);
//
//                tempLog.setRegDateTime(regDateTime);S
                tempLog.setPlanId(log.getString("planId"));
                tempLog.setObjectId(log.getString("objectId"));
                tempLog.setObjectType(log.getString("objectType"));
                tempLog.setObjectAccessLevel(log.getString("objectAccessLevel"));

                allLogs.add(tempLog);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return allLogs;
    }

    public List<Log> getLiveLogList( long planId){
        List<Log> allLogs = new ArrayList<Log>();
        String result = null;
        try {
            GetRequest getRequest = new GetRequest();
            result = getRequest.execute(Environment.URL.getUrl() +"/log/real-time/"+planId).get();
            if(result != null  ){
                if (result.length() != 0) {
                    JSONArray resultArray = new JSONArray(result);
                    for (int i = 0; resultArray.length() > i; i++) {
                        Log tempLog = new Log();
                        JSONObject log = resultArray.getJSONObject(i);
                        tempLog.setId(log.getLong("id"));
                        tempLog.setCoordinateX(BigDecimal.valueOf(log.getDouble("coordinateX")).floatValue());
                        tempLog.setCoordinateY(BigDecimal.valueOf(log.getDouble("coordinateY")).floatValue());

                        tempLog.setPlanId(log.getString("planId"));
                        tempLog.setObjectId(log.getString("objectId"));
                        tempLog.setObjectType(log.getString("objectType"));
                        tempLog.setObjectAccessLevel(log.getString("objectAccessLevel"));

                        allLogs.add(tempLog);
                    }
                }
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return allLogs;
    }

    public List<Log> getLiveLogListByObjectCode(String objectCode){
        List<Log> liveLogs = getLiveLogList(1);
        List<Log> liveObjectLogs = new ArrayList<>();
        for (Log tempLog : liveLogs){
            if(tempLog.getObjectId().equals(objectCode)){
                liveObjectLogs.add(tempLog);
            }
        }
        return  liveObjectLogs;

    }

    public List<Log> getLiveLogListByPlanName(String planId){
        List<Log> liveLogs = getLiveLogList(1);
        List<Log> livePlanLogs = new ArrayList<>();
        List<Log> liveUsersLogs = new ArrayList<>();
        for (Log tempLog : liveLogs){
            if(tempLog.getPlanId().equals(planId)){
                livePlanLogs.add(tempLog);
            }
        }if(!(livePlanLogs.size()==0)) {
            liveUsersLogs.add(livePlanLogs.get(0));
            String userCode = livePlanLogs.get(0).getObjectId();
            for (Log temp : livePlanLogs) {
                if (!userCode.contains(temp.getObjectId())) {
                    userCode = userCode + temp.getObjectId();
                    liveUsersLogs.add(temp);
                }
            }
        }

        return  liveUsersLogs;

    }
}
