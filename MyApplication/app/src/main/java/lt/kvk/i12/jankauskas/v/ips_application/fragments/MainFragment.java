package lt.kvk.i12.jankauskas.v.ips_application.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import lt.kvk.i12.jankauskas.v.ips_application.activities.MainActivity;
import lt.kvk.i12.jankauskas.v.ips_application.models.TracableObject;
import lt.kvk.i12.jankauskas.v.ips_application.services.DeviceDataService;
import lt.kvk.i12.jankauskas.v.ips_application.R;

public class MainFragment extends Fragment {
    private static final String TAG = "MainFragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        Button btnSearchForUser = (Button) view.findViewById(R.id.btnSearchForUser);
        Button btnChoosePlan = (Button) view.findViewById(R.id.btnChoosePlan);




        btnSearchForUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setViewPager(3);
            }
        });

        btnChoosePlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).getPlans();
            }
        });





        return view;

    }






}
