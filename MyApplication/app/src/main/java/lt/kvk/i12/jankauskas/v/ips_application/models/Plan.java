package lt.kvk.i12.jankauskas.v.ips_application.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Plan implements Parcelable {

    protected Plan(Parcel in) {
        id = in.readLong();
        planImage = in.readString();
        planName = in.readString();
        planScale = in.readFloat();
        planWidth = in.readInt();
        planHeight = in.readInt();
    }


    public static final Creator<Plan> CREATOR = new Creator<Plan>() {
        @Override
        public Plan createFromParcel(Parcel in) {
            return new Plan(in);
        }

        @Override
        public Plan[] newArray(int size) {
            return new Plan[size];
        }
    };

    private long id;

    private String planImage;

    private String planName;

    private float planScale;

    private int planWidth;

    private int planHeight;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlanImage() {
        return planImage;
    }

    public void setPlanImage(String planImage) {
        this.planImage = planImage;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public float getPlanScale() {
        return planScale;
    }

    public void setPlanScale(float planScale) {
        this.planScale = planScale;
    }

    public int getPlanWidth() {
        return planWidth;
    }

    public void setPlanWidth(int planWidth) {
        this.planWidth = planWidth;
    }

    public int getPlanHeight() {
        return planHeight;
    }

    public void setPlanHeight(int planHeight) {
        this.planHeight = planHeight;
    }

    public Plan(){

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeLong(id);
        parcel.writeString(planImage);
        parcel.writeString(planName);
        parcel.writeFloat(planScale);
        parcel.writeInt(planWidth);
        parcel.writeInt(planHeight);
    }
}
