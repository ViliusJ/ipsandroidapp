package lt.kvk.i12.jankauskas.v.ips_application.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import lt.kvk.i12.jankauskas.v.ips_application.Environment;
import lt.kvk.i12.jankauskas.v.ips_application.models.TracableObject;
import lt.kvk.i12.jankauskas.v.ips_application.network.GetRequest;

public class TracableObjectAPI {
    public Boolean checkIfObjectCodeExists(String objectCode){
        List<TracableObject> allObjects = getTracableObjectList();
        for(TracableObject object: allObjects){
            if (object != null){
                if(object.getObjectCode().equals(objectCode)){
                    return true;
                }
            }

        }
        return false;
    }

              public List<TracableObject> getTracableObjectList(){
                List<TracableObject> allObjects = new ArrayList<TracableObject>();

                try {
                    GetRequest getRequest = new GetRequest();
                    String result = getRequest.execute(Environment.URL.getUrl()+"/tracked-object").get();
                    if(result != null){
                        JSONArray objects = new JSONArray(result);
                        for (int i = 0; i < objects.length(); i++) {
                            TracableObject tempObject = new TracableObject();
                            JSONObject object = objects.getJSONObject(i);
                            tempObject.setId(object.getLong("id"));
                            tempObject.setAccessLevel(object.getString("objectAccessLevel"));
                            tempObject.setObjectCode(object.getString("objectCode"));
                            tempObject.setObjName(object.getString("objectName"));
                            tempObject.setObjType(object.getString("objectType"));
                            allObjects.add(tempObject);
                        }
                    }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return allObjects;
    }

    public TracableObject getTracableObjectByObjectCode(String objectCode){
        List<TracableObject> allObjects = getTracableObjectList();
        for(TracableObject object: allObjects){
            if (object != null){
                if(object.getObjectCode().equals(objectCode)){
                    return object;
                }
            }

        }

        return null;
    }
}
