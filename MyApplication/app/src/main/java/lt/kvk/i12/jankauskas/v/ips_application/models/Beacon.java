package lt.kvk.i12.jankauskas.v.ips_application.models;

public class Beacon {

    private Long id;

    private String beaconId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public Beacon() {
    }
}
