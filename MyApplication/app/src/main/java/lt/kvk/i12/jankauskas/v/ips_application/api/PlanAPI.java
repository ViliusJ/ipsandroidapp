package lt.kvk.i12.jankauskas.v.ips_application.api;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import lt.kvk.i12.jankauskas.v.ips_application.Environment;
import lt.kvk.i12.jankauskas.v.ips_application.models.Plan;
import lt.kvk.i12.jankauskas.v.ips_application.network.GetRequest;

import static lt.kvk.i12.jankauskas.v.ips_application.services.DeviceDataService.TAG;

public class PlanAPI {

    public List<Plan> getPlanList(){
        List<Plan> allPlans = new ArrayList<Plan>();
        String result = null;
        try {
            GetRequest getRequest = new GetRequest();
            result = getRequest.execute(Environment.URL.getUrl() +"/plan").get();

            JSONArray resultArray = new JSONArray(result);
            for (int i = 0; i < resultArray.length(); i++) {
                Plan tempPlan = new Plan();
                JSONObject plan = resultArray.getJSONObject(i);
                tempPlan.setId(plan.getLong("id"));
                tempPlan.setPlanName(plan.getString("planName"));
                tempPlan.setPlanImage(plan.getString("planImage"));
                tempPlan.setPlanWidth(plan.getInt("planWidth"));
                tempPlan.setPlanHeight(plan.getInt("planHeight"));
                tempPlan.setPlanScale(BigDecimal.valueOf(plan.getDouble("planScale")).floatValue());
                allPlans.add(tempPlan);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return allPlans;
    }

    public Plan getPlanByPlanName( String planName){
        List<Plan> allPlans = getPlanList();
        for (Plan tempPlan : allPlans){
            Log.e(TAG, "Plan data: " + tempPlan.getPlanName());
            if (tempPlan.getPlanName().equals(planName)){
                return tempPlan;
            }
        }
        return null;
    }

    public Plan getPlanById(long planId){
        List<Plan> allPlans = new ArrayList<Plan>();
        String result = null;
        try {
            GetRequest getRequest = new GetRequest();
            result = getRequest.execute(Environment.URL.getUrl() +"/plan/"+planId).get();

                Plan tempPlan = new Plan();
                JSONObject plan = new JSONObject(result);
                tempPlan.setId(plan.getLong("id"));
                tempPlan.setPlanName(plan.getString("planName"));
                tempPlan.setPlanImage(plan.getString("planImage"));
                tempPlan.setPlanWidth(plan.getInt("planWidth"));
                tempPlan.setPlanHeight(plan.getInt("planHeight"));
                tempPlan.setPlanScale(BigDecimal.valueOf(plan.getDouble("planScale")).floatValue());
                allPlans.add(tempPlan);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return allPlans.get(0);
    }

}
