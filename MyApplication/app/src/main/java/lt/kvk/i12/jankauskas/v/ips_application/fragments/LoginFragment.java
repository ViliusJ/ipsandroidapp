package lt.kvk.i12.jankauskas.v.ips_application.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import lt.kvk.i12.jankauskas.v.ips_application.activities.MainActivity;
import lt.kvk.i12.jankauskas.v.ips_application.R;
import lt.kvk.i12.jankauskas.v.ips_application.api.TracableObjectAPI;


public class LoginFragment extends Fragment {
    private static final String TAG = "LoginFragment";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        final TracableObjectAPI  tracableObjectAPI = new TracableObjectAPI();


        Button btn = (Button) view.findViewById(R.id.btnLogIn);
        final EditText textField = (EditText) view.findViewById(R.id.txtObjectCode);



        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if(((MainActivity)getActivity()).checkForInternetConection()){
                        ((MainActivity) getActivity()).userLogIn(textField.getText().toString());
                    }else{
                        Toast.makeText(getActivity(), "Please connect to internet",Toast.LENGTH_SHORT).show();
                    }






            }
        });

        return view;
    }



}
