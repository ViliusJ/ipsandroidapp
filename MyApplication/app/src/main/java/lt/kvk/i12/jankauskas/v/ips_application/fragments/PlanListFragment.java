package lt.kvk.i12.jankauskas.v.ips_application.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import lt.kvk.i12.jankauskas.v.ips_application.R;
import lt.kvk.i12.jankauskas.v.ips_application.activities.MainActivity;
import lt.kvk.i12.jankauskas.v.ips_application.activities.MapActivityPlan;
import lt.kvk.i12.jankauskas.v.ips_application.api.PlanAPI;

public class PlanListFragment extends Fragment {

    PlanAPI planAPI;
    ListView listView;
    String[] values;
    private static final String TAG = "PlanListFragment";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_plan_list, container, false);

        planAPI = new PlanAPI();

       listView = (ListView) view.findViewById(R.id.list);




        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {

             values = ((MainActivity)getActivity()).getPlanNames();
//            values = new tring[]{"test","test2","test3"};

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(((MainActivity)getActivity()),
                    android.R.layout.simple_list_item_1, android.R.id.text1, values);

            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    for (int i = 0; values.length> i; i++){
                        if (position == i) {
                            Intent myIntent = new Intent(view.getContext(), MapActivityPlan.class);

                            myIntent.putExtra("Plan Id", ((MainActivity)getActivity()).getPlanIdByName(values[i]));
                            startActivityForResult(myIntent, 0);
                        }
                    }

                }
            });

        }
    }



}
