package lt.kvk.i12.jankauskas.v.ips_application.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import lt.kvk.i12.jankauskas.v.ips_application.api.ViolationAPI;
import lt.kvk.i12.jankauskas.v.ips_application.fragments.FragmentAdapter;
import lt.kvk.i12.jankauskas.v.ips_application.R;
import lt.kvk.i12.jankauskas.v.ips_application.api.PlanAPI;
import lt.kvk.i12.jankauskas.v.ips_application.api.TracableObjectAPI;
import lt.kvk.i12.jankauskas.v.ips_application.fragments.LoginFragment;
import lt.kvk.i12.jankauskas.v.ips_application.fragments.MainFragment;
import lt.kvk.i12.jankauskas.v.ips_application.fragments.PlanListFragment;
import lt.kvk.i12.jankauskas.v.ips_application.fragments.SearchFragment;
import lt.kvk.i12.jankauskas.v.ips_application.models.Plan;
import lt.kvk.i12.jankauskas.v.ips_application.models.TracableObject;
import lt.kvk.i12.jankauskas.v.ips_application.models.Violation;
import lt.kvk.i12.jankauskas.v.ips_application.services.DeviceDataService;
//import com.crashlytics.android.Crashlytics;
//import io.fabric.sdk.android.Fabric;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private DrawerLayout drawer;
    private TracableObjectAPI tracableObjectAPI;
    private PlanAPI planAPI;
    BluetoothManager btManager;
    BluetoothAdapter btAdapter;
    BluetoothLeScanner btScanner;

    private final static int REQUEST_ENABLE_BT = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    private FragmentAdapter fragmentAdapter;
    private ViewPager viewPager;
    private TracableObject user;
    private String[] planNames;
    private ArrayList<Plan> planList;
    private ViolationAPI violationAPI;
    public volatile boolean violationCheck = true;
    private NavigationView navigationView;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.quick_menu, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.keisti_naudotoja:
                setViewPager(0);
                break;
            case R.id.pagrindinis:
                setViewPager(1);
                break;
            case R.id.dabartine_vieta:
                View view = new View(this);
                showMapActivity(view);
                break;
            case R.id.naudotojo_paieska:
                setViewPager(3);
                break;
            case R.id.pasirinkti_pastata:
                getPlans();
                break;
            case android.R.id.home:
                setViewPager(1);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.pradėti:
                startService(new Intent(this, DeviceDataService.class));
                TracableObject user = getCurrentUser();
                Intent intent = new Intent(this, DeviceDataService.class);
                intent.putExtra("user", user);
                startService(intent);
                return true;
            case R.id.sustabdyti:
                stopService(new Intent(this, DeviceDataService.class));
                return true;




        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        violationAPI = new ViolationAPI();
        planAPI = new PlanAPI();
        tracableObjectAPI = new TracableObjectAPI();

        fragmentAdapter = new FragmentAdapter(getSupportFragmentManager());
        viewPager = (ViewPager)findViewById(R.id.container);
        //setup the pager
        setupViewPager(viewPager);
        if(getCurrentUser().getObjectCode().length()!=0){

            View v = navigationView.getHeaderView(0);
            TextView textView= (TextView ) v.findViewById(R.id.textViewUser);
            textView.setText(getCurrentUser().getObjName());
            checkForViolations(getCurrentUser().getObjectCode());
            setViewPager(1);
        }

        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();
        btScanner = btAdapter.getBluetoothLeScanner();

        if (btAdapter != null && !btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent,REQUEST_ENABLE_BT);
        }

        // Make sure we have access coarse location enabled, if not, prompt the user to enable it
        if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("This app needs location access");
            builder.setMessage("Please grant location access so this app can detect peripherals.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                }
            });
            builder.show();
        }




    } @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    AlertDialog.Builder functionality_limited = builder.setTitle("Functionality limited");
                    builder.setMessage("Background scanning not available");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }



    private void setupViewPager(ViewPager pager){
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        adapter.addFragment(new LoginFragment(), "LoginFragment");
        adapter.addFragment(new MainFragment(), "MainFragment");
        adapter.addFragment(new PlanListFragment(), "PlanListFragment");
        adapter.addFragment(new SearchFragment(), "SearchFragment");
        pager.setAdapter(adapter);

    }

    public void setViewPager(int fragment){
        viewPager.setCurrentItem(fragment);
    }

    public boolean checkForInternetConection(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else{
            connected = false;}
            return connected;
    }

    public TracableObject getCurrentUser() {
        TracableObject tempUser = new TracableObject();
        long tempID = 0;
        String tempNAME ="";
        String tempCODE ="";
        String tempACCESS ="";
        String tempTYPE ="";
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        tempUser.setId(sharedPref.getLong("ID",tempID));
        tempUser.setObjName(sharedPref.getString("NAME",tempNAME));
        tempUser.setObjectCode(sharedPref.getString("CODE",tempCODE));
        tempUser.setAccessLevel(sharedPref.getString("ACCESS",tempACCESS));
        tempUser.setObjType(sharedPref.getString("TYPE",tempTYPE));
        Toast.makeText(MainActivity.this, "User from memory : "+ tempUser.getObjectCode(),Toast.LENGTH_SHORT).show();


        return tempUser;
    }

    public void setCurrentUser(TracableObject currentUser) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("ID", currentUser.getId());
        editor.putString("NAME", currentUser.getObjName());
        editor.putString("CODE", currentUser.getObjectCode());
        editor.putString("ACCESS", currentUser.getAccessLevel());
        editor.putString("TYPE", currentUser.getObjType());
        editor.commit();
    }

    public void showMapActivity(View view) {
        if(checkForInternetConection()){
            Intent intentActivity = new Intent(this, MapActivityUser.class);
            startActivity(intentActivity );
        }else {
            Toast.makeText(this, "Please connect to internet",Toast.LENGTH_SHORT).show();
        }


    }

    private BroadcastReceiver violatioReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive( Context context, Intent intent) {


            Toast.makeText(context, "YOU ARE IN RESTRICTED AREA",Toast.LENGTH_SHORT).show();





        }
    };

    public void checkForViolations(final String objectCode){
        Thread t = new Thread(){
            public void run(){
                while (true){
                    if(!violationCheck){
                        finish();
                    }
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    List<Violation> violations = new ArrayList<>();
                    violations = violationAPI.getNewViolationByObjectCode(objectCode);

                    if(violations.size()> 0){
                        Intent intent = new Intent("lt.kvk.i12.jankauskas.v.VIOLATION_SERVICE");
                        sendBroadcast(intent);
                    }

                }


            }

        };t.start();

    }

    private BroadcastReceiver logInReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            TracableObject user = intent.getParcelableExtra("User");
            if(user!= null){

                setCurrentUser(user);
                Log.i("PrintOut" , "Object exists");
                stopService(new Intent(context, DeviceDataService.class));
                startService(new Intent(context, DeviceDataService.class));
//                TracableObject tempUser = getCurrentUser();
                Intent serviceIntent = new Intent(context, DeviceDataService.class);
//                serviceIntent.putExtra("user", user);
                startService(serviceIntent);
                View v = navigationView.getHeaderView(0);
                TextView textView= (TextView ) v.findViewById(R.id.textViewUser);
                textView.setText(user.getObjName());
                checkForViolations(user.getObjectCode());
                setViewPager(1);
            }else{
                Toast.makeText(context, "User not found",Toast.LENGTH_SHORT).show();
            }




        }
    };

    public void userLogIn(final String objectCode){

        if(checkForInternetConection()){
            Thread t = new Thread()
            {
                public void run() {

                    TracableObject user = tracableObjectAPI.getTracableObjectByObjectCode(objectCode);
                    Intent intent = new Intent("lt.kvk.i12.jankauskas.v.LOGIN_SERVICE");
                    intent.putExtra("User", user);
                    sendBroadcast(intent);}

            };t.start();

        }else {
            Toast.makeText(this, "Please connect to internet",Toast.LENGTH_SHORT).show();
        }

        }

    private BroadcastReceiver planListReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ArrayList<Plan> allPlans = intent.getParcelableArrayListExtra("All plans");
            if(allPlans.size() ==0){
                noPlans();
            }
            else {setPlanList(allPlans);
                setPlanNames(allPlans);
                setViewPager(2);}

        }
    };

    public void noPlans(){
        Toast.makeText(this, "Problem with server, no maps found",Toast.LENGTH_SHORT).show();
    }



    public void getPlans(){
        if(checkForInternetConection()){
            Thread t = new Thread()
            {
                public void run() {

                    ArrayList<Plan> allPlans = new ArrayList<>(planAPI.getPlanList());
                    Intent intent = new Intent("lt.kvk.i12.jankauskas.v.PLAN_LIST_SERVICE");
                    intent.putParcelableArrayListExtra("All plans", allPlans);
                    sendBroadcast(intent);}

            };t.start();
        }else {
            Toast.makeText(this, "Please connect to internet",Toast.LENGTH_SHORT).show();
        }

    }
    private BroadcastReceiver searchReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive( Context context, Intent intent) {

            TracableObject tempUser = intent.getParcelableExtra("User");
            if(tempUser!= null){
                setUser(tempUser);
                Intent myIntent = new Intent(context, MapActivitySearchUser.class);

                myIntent.putExtra("User", tempUser);
                context.startActivity(myIntent );


            }else{
                Toast.makeText(context, "User not found",Toast.LENGTH_SHORT).show();
            }




        }
    };


    public void getUser(final String objectCode){
        if(checkForInternetConection()){
            Thread t = new Thread()
            {
                public void run() {

                    TracableObject user = tracableObjectAPI.getTracableObjectByObjectCode(objectCode);
                    Intent intent = new Intent("lt.kvk.i12.jankauskas.v.SEARCH_SERVICE");
                    intent.putExtra("User", user);
                    sendBroadcast(intent);}

            };t.start();

        }else {
            Toast.makeText(this, "Please connect to internet",Toast.LENGTH_SHORT).show();
        }


    }

    public long getPlanIdByName( String planName) {
        for (Plan planTemp: this.planList){
            if(planTemp.getPlanName().equals(planName)){
                return planTemp.getId();
            }
        }
        return 1;
    }

    public String[] getPlanNames() {
        return planNames;
    }

    public void setPlanNames(ArrayList<Plan> planList) {
        this.planNames = new String[planList.size()+1];
        planNames[0] = "non";
        for(int i =1; planList.size()>=i;i++){
            this.planNames[i] = planList.get(i-1).getPlanName();
        }

    }

    public void setPlanList(ArrayList<Plan> planList) {
        this.planList = planList;
    }

    public TracableObject getUser() {
        return user;
    }

    public void setUser(TracableObject user) {
        this.user = user;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume()
    {
        IntentFilter violationFilter = new IntentFilter("lt.kvk.i12.jankauskas.v.VIOLATION_SERVICE");
        registerReceiver(violatioReceiver,violationFilter);

        IntentFilter logFilter = new IntentFilter("lt.kvk.i12.jankauskas.v.LOGIN_SERVICE");
        registerReceiver(logInReceiver,logFilter);

        IntentFilter planFilter = new IntentFilter("lt.kvk.i12.jankauskas.v.PLAN_LIST_SERVICE");
        registerReceiver(planListReceiver,planFilter);

        IntentFilter searchFilter = new IntentFilter("lt.kvk.i12.jankauskas.v.SEARCH_SERVICE");
        registerReceiver(searchReceiver,searchFilter);
        super.onResume();

    }
    //
    @Override
    protected void onPause() {
        unregisterReceiver(violatioReceiver);
        unregisterReceiver(logInReceiver);
        unregisterReceiver(planListReceiver);
        unregisterReceiver(searchReceiver);

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        violationCheck = false;
        super.onDestroy();
    }
}

