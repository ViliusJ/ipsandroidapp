package lt.kvk.i12.jankauskas.v.ips_application.models;

public class BeaconInPlan {
    private Long id;

    private String beaconId;

    private Float beaconCoordinateX;

    private Float beaconCoordinateY;

    private String plandId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public Float getBeaconCoordinateX() {
        return beaconCoordinateX;
    }

    public void setBeaconCoordinateX(Float beaconCoordinateX) {
        this.beaconCoordinateX = beaconCoordinateX;
    }

    public Float getBeaconCoordinateY() {
        return beaconCoordinateY;
    }

    public void setBeaconCoordinateY(Float beaconCoordinateY) {
        this.beaconCoordinateY = beaconCoordinateY;
    }

    public String getPlandId() {
        return plandId;
    }

    public void setPlandId(String plandId) {
        this.plandId = plandId;
    }

    public BeaconInPlan() {
    }
}
