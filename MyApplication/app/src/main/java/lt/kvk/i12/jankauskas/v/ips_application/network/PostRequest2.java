package lt.kvk.i12.jankauskas.v.ips_application.network;

import android.util.Log;

import com.kontakt.sdk.android.common.profile.ISecureProfile;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Random;

import lt.kvk.i12.jankauskas.v.ips_application.Environment;
import lt.kvk.i12.jankauskas.v.ips_application.models.TracableObject;

public class PostRequest2 {
    private Exception exception;



    public void sendPostDeviceData(final List<ISecureProfile> deviceData, final TracableObject user) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(Environment.URL.getUrl()+"/device-data");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("PUT");
                    conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    conn.setRequestProperty("Accept", "application/json");
//                    conn.setDoOutput(true);
//                    conn.setDoInput(true);
                    Random rand = new Random();

                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("objectId1", "55ss");
                    jsonParam.put("objectId2", "55ss");
                    jsonParam.put("objectId3", "55ss");
                    jsonParam.put("signal1", deviceData.get(0).getRssi()-5);
                    jsonParam.put("signal2", deviceData.get(1).getRssi()-5);
                    jsonParam.put("signal3", deviceData.get(2).getRssi()+5);
                    jsonParam.put("TransmitterId1", deviceData.get(0).getUniqueId());
                    jsonParam.put("TransmitterId2", deviceData.get(1).getUniqueId());
                    jsonParam.put("TransmitterId3", deviceData.get(2).getUniqueId());
                    jsonParam.put("TxPower1", deviceData.get(0).getTxPower());
                    jsonParam.put("TxPower2", deviceData.get(1).getTxPower());
                    jsonParam.put("TxPower3", deviceData.get(2).getTxPower());
                    Log.i("JSON", jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                    //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();

                    Log.i("STATUS", String.valueOf(conn.getResponseCode()));
//                    Log.i("MSG" , conn.getResponseMessage());

                    conn.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

}
