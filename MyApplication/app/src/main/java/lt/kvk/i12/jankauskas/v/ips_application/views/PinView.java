package lt.kvk.i12.jankauskas.v.ips_application.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.util.ArrayList;

import lt.kvk.i12.jankauskas.v.ips_application.R;


public class PinView extends SubsamplingScaleImageView  {

    private ArrayList<PointF> uPin = new ArrayList<>();
    private ArrayList<String> pinNames = new ArrayList<>();
    private ArrayList<Bitmap> pins = new ArrayList<>();
    private Bitmap pin;
    private ArrayList<PointF> oPin = new ArrayList<>();
    private ArrayList<String> oPinNames = new ArrayList<>();
    private ArrayList<Bitmap> oPins = new ArrayList<>();
    private Bitmap objectPin;
    private ArrayList<PointF> sPin = new ArrayList<>();
    private ArrayList<String> sPinNames = new ArrayList<>();
    private ArrayList<Bitmap> sPins = new ArrayList<>();
    private Bitmap searchedPin;

    public PinView(Context context) {
        this(context, null);
    }

    public PinView(Context context, AttributeSet attr) {
        super(context, attr);
        initialiseUser();
    }

    public boolean setPinUser(PointF uPin, String name) {
        if (pinNames.contains(name)){
            return false;
        } else {
            this.uPin.add(uPin);
            pinNames.add(name);
            initialiseUser();
            invalidate();
            return true;
        }
    }

    public boolean setPinObject(PointF oPin , String name) {
        if (oPinNames.contains(name)){
            return false;
        } else {
            this.oPin.add(oPin);
            oPinNames.add(name);
            initialiseObject();
            invalidate();
            return true;
        }
    }

    public boolean setPinSearched(PointF sPin , String name) {
        if (sPinNames.contains(name)){
            return false;
        } else {
            this.sPin.add(sPin);
            sPinNames.add(name);
            initialiseSearched();
            invalidate();
            return true;
        }
    }

    public PointF getPin(String name) {

        return uPin.get(pinNames.indexOf(name));
    }

    public void updateUPin(PointF pin , String name){
        PointF point = new PointF();
        point.x = pin.x;
        point.y = pin.y;
        uPin.set(pinNames.indexOf(name),pin);
        initialiseUser();
        invalidate();
    }

    public void updateOPin(PointF pin , String name){
        PointF point = new PointF();
        point.x = pin.x;
        point.y = pin.y;
        oPin.set(oPinNames.indexOf(name),pin);
        initialiseUser();
        invalidate();
    }

    public void updateSPin(PointF pin , String name){
        PointF point = new PointF();
        point.x = pin.x;
        point.y = pin.y;
        sPin.set(sPinNames.indexOf(name),pin);
        initialiseUser();
        invalidate();
    }


    public boolean removeUPin(String name){
        if (pinNames.contains(name)){
            pins.remove(pinNames.indexOf(name));
            uPin.remove(pinNames.indexOf(name));
            pinNames.remove(name);
            return true;
        } else {
            return false;
        }
    }

    public boolean removeOPin(String name){
        if (oPinNames.contains(name)){
            oPins.remove(oPinNames.indexOf(name));
            oPin.remove(oPinNames.indexOf(name));
            oPinNames.remove(name);
            return true;
        } else {
            return false;
        }
    }

    public boolean removeSPin(String name){
        if (sPinNames.contains(name)){
            sPins.remove(sPinNames.indexOf(name));
            sPin.remove(sPinNames.indexOf(name));
            sPinNames.remove(name);
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<String> getPinNames(){
        return pinNames;
    }

    private void initialiseUser() {
        float density = getResources().getDisplayMetrics().densityDpi;
        pin = BitmapFactory.decodeResource(this.getResources(), R.drawable.marker_red);
        float w = (density/420f) * pin.getWidth();
        float h = (density/420f) * pin.getHeight();
        pin = Bitmap.createScaledBitmap(pin, (int)w, (int)h, true);
        this.pins.add(pin);
    }

    private void initialiseObject() {
        float density = getResources().getDisplayMetrics().densityDpi;
        objectPin = BitmapFactory.decodeResource(this.getResources(), R.drawable.marker_blue);
        float w = (density/420f) * objectPin.getWidth();
        float h = (density/420f) * objectPin.getHeight();
        objectPin = Bitmap.createScaledBitmap(objectPin, (int)w, (int)h, true);
        this.oPins.add(objectPin);
    }

    private void initialiseSearched() {
        float density = getResources().getDisplayMetrics().densityDpi;
        searchedPin = BitmapFactory.decodeResource(this.getResources(), R.drawable.marker_green);
        float w = (density/420f) * searchedPin.getWidth();
        float h = (density/420f) * searchedPin.getHeight();
        searchedPin= Bitmap.createScaledBitmap(searchedPin, (int)w, (int)h, true);
        this.sPins.add(searchedPin);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Don't draw pin before image is ready so it doesn't move around during setup.
        if (!isReady()) {
            return;
        }

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        for (int i=0;i<uPin.size(); i++){
            if (uPin.get(i) != null && pins.get(i) != null) {
                PointF vPin = sourceToViewCoord(uPin.get(i));
                float vX = vPin.x - (pins.get(i).getWidth()/2);
                float vY = vPin.y - pins.get(i).getHeight();
                canvas.drawBitmap(pins.get(i), vX, vY, paint);
            }
        }
        for (int i=0;i<oPin.size(); i++){
            if (oPin.get(i) != null && oPins.get(i) != null) {
                PointF vPin = sourceToViewCoord(oPin.get(i));
                float vX = vPin.x - (oPins.get(i).getWidth()/2);
                float vY = vPin.y - oPins.get(i).getHeight();
                canvas.drawBitmap(oPins.get(i), vX, vY, paint);
            }
        }

        for (int i=0;i<sPin.size(); i++){
            if (sPin.get(i) != null && sPins.get(i) != null) {
                PointF vPin = sourceToViewCoord(sPin.get(i));
                float vX = vPin.x - (sPins.get(i).getWidth()/2);
                float vY = vPin.y - sPins.get(i).getHeight();
                canvas.drawBitmap(sPins.get(i), vX, vY, paint);
            }
        }
    }

}