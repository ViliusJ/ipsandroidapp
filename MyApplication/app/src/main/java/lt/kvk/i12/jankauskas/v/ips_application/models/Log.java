package lt.kvk.i12.jankauskas.v.ips_application.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Log implements Parcelable {

    protected Log(Parcel in) {
        id = in.readLong();
        planId = in.readString();
        coordinateX = in.readFloat();
        coordinateY = in.readFloat();
        objectId = in.readString();
        objectName = in.readString();
        objectType = in.readString();
        objectAccessLevel = in.readString();
    }


    public static final Creator<Log> CREATOR = new Creator<Log>() {
        @Override
        public Log createFromParcel(Parcel in) {
            return new Log(in);
        }

        @Override
        public Log[] newArray(int size) {
            return new Log[size];
        }
    };

    private long id;

    private String planId ;

    private Float coordinateX;

    private Float coordinateY;

    private Date regDateTime;

    private String objectId;

    private String objectName;

    private String objectType;

    private String objectAccessLevel;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public Float getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(Float coordinateX) {
        this.coordinateX = coordinateX;
    }

    public Float getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(Float coordinateY) {
        this.coordinateY = coordinateY;
    }

    public Date getRegDateTime() {
        return regDateTime;
    }

    public void setRegDateTime(Date regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectAccessLevel() {
        return objectAccessLevel;
    }

    public void setObjectAccessLevel(String objectAccessLevel) {
        this.objectAccessLevel = objectAccessLevel;
    }

    public Log() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeLong(id);
        parcel.writeString(planId);
        parcel.writeFloat(coordinateX);
        parcel.writeFloat(coordinateY);
        parcel.writeString(objectId);
        parcel.writeString(objectName);
        parcel.writeString(objectType);
        parcel.writeString(objectAccessLevel);

    }
}
