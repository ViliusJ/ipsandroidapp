package lt.kvk.i12.jankauskas.v.ips_application.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import lt.kvk.i12.jankauskas.v.ips_application.Environment;
import lt.kvk.i12.jankauskas.v.ips_application.models.Violation;
import lt.kvk.i12.jankauskas.v.ips_application.network.GetRequest;

public class ViolationAPI {



    public List<Violation> getViolationList(){
        List<Violation> allObjects = new ArrayList<Violation>();

        try {
            GetRequest getRequest = new GetRequest();
            String result = getRequest.execute(Environment.URL.getUrl()+"/violations").get();
            if(result != null && result !=""){
                JSONArray objects = new JSONArray(result);
                for (int i = 0; i < objects.length(); i++) {
                    Violation tempObject = new Violation();
                    JSONObject object = objects.getJSONObject(i);
                    tempObject.setId(object.getLong("id"));
                    tempObject.setPlanName(object.getString("PlanName"));
                    tempObject.setRestrictedArea(object.getString("RestrictedArea"));

                    String dateStr = object.getString("ViolationDateTime");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Date violationDate = null;
                    try {
                        violationDate = sdf.parse(dateStr );
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    tempObject.setViolationDateTime(violationDate);
                    tempObject.setObjectAccessLevel(object.getString("objectAccessLevel"));
                    tempObject.setObjectCode(object.getString("objectCode"));
                    tempObject.setObjectName(object.getString("objectName"));
                    tempObject.setObjectType(object.getString("objectType"));
                    allObjects.add(tempObject);
                }
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return allObjects;
    }

    public List<Violation> getNewViolationByObjectCode(String objectCode){
        List<Violation> violations = new ArrayList<>();
        List<Violation> allViolations = getViolationList();
        for (Violation temp: allViolations){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            Date currentDate = new Date();

            Date pastDate = new Date(System.currentTimeMillis() - ( 10 * 1000));
            if ((temp.getViolationDateTime().compareTo(currentDate) <0) &&(temp.getViolationDateTime().compareTo(pastDate) >0)) {

                if(temp.getObjectCode().equals(objectCode)){
                    violations.add(temp);
                    return violations;
                }

            }


            }
        return violations;
    }
}


