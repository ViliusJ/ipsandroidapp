package lt.kvk.i12.jankauskas.v.ips_application.models;

public class DeviceData {

    private String objectId1;

    private String objectId2;

    private String objectId3;

    private Float signal1;

    private Float signal2;

    private Float signal3;

    private String transmitterId1;

    private String transmitterId2;

    private String transmitterId3;

    public String getObjectId1() {
        return objectId1;
    }

    public void setObjectId1(String objectId1) {
        this.objectId1 = objectId1;
    }

    public String getObjectId2() {
        return objectId2;
    }

    public void setObjectId2(String objectId2) {
        this.objectId2 = objectId2;
    }

    public String getObjectId3() {
        return objectId3;
    }

    public void setObjectId3(String objectId3) {
        this.objectId3 = objectId3;
    }

    public Float getSignal1() {
        return signal1;
    }

    public void setSignal1(Float signal1) {
        this.signal1 = signal1;
    }

    public Float getSignal2() {
        return signal2;
    }

    public void setSignal2(Float signal2) {
        this.signal2 = signal2;
    }

    public Float getSignal3() {
        return signal3;
    }

    public void setSignal3(Float signal3) {
        this.signal3 = signal3;
    }

    public String getTransmitterId1() {
        return transmitterId1;
    }

    public void setTransmitterId1(String transmitterId1) {
        this.transmitterId1 = transmitterId1;
    }

    public String getTransmitterId2() {
        return transmitterId2;
    }

    public void setTransmitterId2(String transmitterId2) {
        this.transmitterId2 = transmitterId2;
    }

    public String getTransmitterId3() {
        return transmitterId3;
    }

    public void setTransmitterId3(String transmitterId3) {
        this.transmitterId3 = transmitterId3;
    }

    public DeviceData() {
    }
}
