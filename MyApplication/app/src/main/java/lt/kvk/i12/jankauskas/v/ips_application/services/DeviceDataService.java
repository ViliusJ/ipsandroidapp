package lt.kvk.i12.jankauskas.v.ips_application.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import com.kontakt.sdk.android.ble.configuration.ScanMode;
import com.kontakt.sdk.android.ble.configuration.ScanPeriod;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.common.KontaktSDK;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;

import lt.kvk.i12.jankauskas.v.ips_application.models.TracableObject;
import lt.kvk.i12.jankauskas.v.ips_application.network.PostRequest;
import lt.kvk.i12.jankauskas.v.ips_application.network.PostRequest2;


public class DeviceDataService extends Service {


    public static final String TAG = "ProximityManager";
    private ProximityManager proximityManager;
    PostRequest connection = new PostRequest();
    PostRequest2 connection2 = new PostRequest2();
    TracableObject user;

    @Override
    public IBinder onBind(Intent intent) {
        return  null;
    }

    @Override
    public void onCreate() {
        //Device uses sdk to scan for specific Bluetooth devices
        KontaktSDK.initialize("YkcAlNxrEajVqziCLhUikzJpOgHwjtFy");
        setupProximityManager();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        user = getCurrentUser();
        startScanning();
        return START_STICKY;
    }

    public boolean checkForInternetConection(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else{
            connected = false;}
        return connected;
    }

    public TracableObject getCurrentUser() {
        TracableObject tempUser = new TracableObject();
        long tempID = 0;
        String tempNAME ="";
        String tempCODE ="";
        String tempACCESS ="";
        String tempTYPE ="";
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        tempUser.setId(sharedPref.getLong("ID",tempID));
        tempUser.setObjName(sharedPref.getString("NAME",tempNAME));
        tempUser.setObjectCode(sharedPref.getString("CODE",tempCODE));
        tempUser.setAccessLevel(sharedPref.getString("ACCESS",tempACCESS));
        tempUser.setObjType(sharedPref.getString("TYPE",tempTYPE));

        Toast.makeText(DeviceDataService.this, "User from memory : "+ tempUser.getObjectCode(),Toast.LENGTH_SHORT).show();

        return tempUser;
    }

    private void setupProximityManager() {
        proximityManager = ProximityManagerFactory.create(this);
        //Configure proximity manager basic options
        proximityManager.configuration()
                //Using ranging for continuous scanning or MONITORING for scanning with intervals
                .scanPeriod(ScanPeriod.RANGING)
                //Using BALANCED for best performance/battery ratio
                .scanMode(ScanMode.BALANCED)
                //OnDeviceUpdate callback will be received with 0.5 seconds interval
                .deviceUpdateCallbackInterval((int)100);

        //Setting up Secure Profile listener
//        proximityManager.setSecureProfileListener(createSecureProfileListener());
        proximityManager.setIBeaconListener(createIBeaconListener());
    }


    private void startScanning() {
        //Connect to scanning service and start scanning when ready
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                //Check if proximity manager is already scanning
                if (proximityManager.isScanning()) {
                    Toast.makeText(DeviceDataService.this, "Already scanning", Toast.LENGTH_SHORT).show();
                    return;
                }
                proximityManager.startScanning();
                Toast.makeText(DeviceDataService.this, "Started scanning", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private IBeaconListener createIBeaconListener() {
        return new IBeaconListener() {
            @Override
            public void onIBeaconDiscovered(IBeaconDevice iBeacon, IBeaconRegion region) {
                Log.i(TAG, "onIBeaconDiscovered: " + iBeacon.toString());
            }

            @Override
            public void onIBeaconsUpdated(List<IBeaconDevice> list, IBeaconRegion region) {
                List<IBeaconDevice> iBeacon = new ArrayList<>();
                Log.i(TAG, "onIBeaconsUpdated: " + list.size());
                if (list.size() >= 3){
                    for(IBeaconDevice temp: list){
                        if(!(temp.getUniqueId() == null)){
                            iBeacon.add(temp);
                            Log.e(TAG, "Beacon variable: " + temp.toString());
                        }
                    }
                    if(iBeacon.size() >= 3){
                        connection.sendPostDeviceData(iBeacon, user);
                    }


                }
            }

            @Override
            public void onIBeaconLost(IBeaconDevice iBeacon, IBeaconRegion region) {
                Log.e(TAG, "onIBeaconLost: " + iBeacon.toString());
            }
        };
    }

//    private void startScanning() {
//        //Connect to scanning service and start scanning when ready
//        proximityManager.connect(new OnServiceReadyListener() {
//            @Override
//            public void onServiceReady() {
//                //Check if proximity manager is already scanning
//                if (proximityManager.isScanning()) {
//                    Toast.makeText(DeviceDataService.this, "Already scanning", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                proximityManager.startScanning();
//                Toast.makeText(DeviceDataService.this, "Scanning started", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    //Profile that lisens for specific Bluetooth devices
//    private SecureProfileListener createSecureProfileListener() {
//        return new SecureProfileListener() {
//            @Override
//            public void onProfileDiscovered(ISecureProfile iSecureProfile) {
//            }
//
//            @Override
//            public void onProfilesUpdated(List<ISecureProfile> list) {
//                Log.i(TAG, "onProfilesUpdated: " + list.size());
//                Log.i(TAG, "Beacon: " + list.get(0));
//                if (list.size() >= 3){
//
//                    if (checkForInternetConection()){
//
//                        connection.sendPostDeviceData(list, user);
//                        connection2.sendPostDeviceData(list, user);
//                    }
//
//                }
//            }
//
//            @Override
//            public void onProfileLost(ISecureProfile iSecureProfile) {
//            }
//        };
//    }




    @Override
    public void onDestroy() {

        proximityManager.disconnect();
        super.onDestroy();
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
    }
}