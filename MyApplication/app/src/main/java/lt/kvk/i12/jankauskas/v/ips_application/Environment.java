package lt.kvk.i12.jankauskas.v.ips_application;

public enum Environment {
    URL("http://192.168.0.182:8080/api");


    private String url;

    Environment(String envUrl) {
        this.url = envUrl;
    }

    public String getUrl() {
        return url;
    }
}
