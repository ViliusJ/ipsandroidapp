package lt.kvk.i12.jankauskas.v.ips_application.models;

//import lombok.Data;
import android.os.Parcel;
import android.os.Parcelable;


public class TracableObject implements Parcelable {
    private long id;

    private String objectCode;

    private String objType;

    private String objName;

    private String accessLevel;

    protected TracableObject(Parcel in) {
        id = in.readLong();
        objectCode = in.readString();
        objType = in.readString();
        objName = in.readString();
        accessLevel = in.readString();
    }

    public static final Creator<TracableObject> CREATOR = new Creator<TracableObject>() {
        @Override
        public TracableObject createFromParcel(Parcel in) {
            return new TracableObject(in);
        }

        @Override
        public TracableObject[] newArray(int size) {
            return new TracableObject[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }

    public String getObjType() {
        return objType;
    }

    public void setObjType(String objType) {
        this.objType = objType;
    }

    public String getObjName() {
        return objName;
    }

    public void setObjName(String objName) {
        this.objName = objName;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public TracableObject(String objectId, String objType, String objName, String accessLevel) {
        this.objectCode = objectCode;
        this.objType = objType;
        this.objName = objName;
        this.accessLevel = accessLevel;
    }
    public TracableObject(){}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(objectCode);
        parcel.writeString(objType);
        parcel.writeString(objName);
        parcel.writeString(accessLevel);
    }
}
